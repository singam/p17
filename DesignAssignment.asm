.model small
.data
	org 03FCh
	ip dw 1000h
	cs dw 0000h
	;; these will be stored from 3000h
	org 3000h
	frequency dw 0000h
	form db 00h

org 0400h ; moving code segment to 0400h
.code
.startup
	;initializing the PPI
	mov al, 90h
	OUT 86h, al
	;initializing the data to DAC and waveform to 0
	mov al, 00h
	out 82h, al  ;PORT B is set to 0
	out 84h, al ;PORT c is set to 0
	disp:

		mov bx, frequency;
		mov cl,10h

		mov al,bl
		and al,0Fh
		add al,00h ;select the first display
		out 84h,al
		CALL delay

		mov al,bl
		and ax,00F0h
		div cl
		add al,10h ;select the second display
		out 84h,al
		CALL delay

		mov al,bh
		and al,0Fh
		add al,20h  ;select the third display
		out 84h,al
		CALL delay

		mov al,bh
		and ax,00F0h
		div cl
		add al,30h  ;select the fourth display
		out 84h,al
		CALL delay

	jmp disp ;cause the display to happen in an infinite loop
.exit

	DELAY proc near  ; gives a delay of about 2ms
	pushf
	mov cx, 435 
	X1:
		NOP
		NOP
		loop X1
	ret
	DELAY endp

; external interrupt service routine
org 1000h
ISR:
	in al, 80h  ; buttons connected to Port A
	
	; 
	cmp al, 0FEh ;chk if the 10 button was pressed
	jz ten
	cmp al, 0FDh  ;chk if the 100 button was pressed
	jz hundred
	cmp al, 0FBh  ;chk if the 1K button was pressed
	jz onek
	cmp al, 0F7h  ;chk if the 10K button was pressed
	jz tenk
	
	cmp al, 0EFh  ;chk if the sine button was pressed
	jz sine
	cmp al, 0DFh  ;chk if the tri button was pressed
	jz tri
	cmp al, 0BFh  ;chk if the square button was pressed
	jz square
	
	cmp al, 7Fh  ;chk if the generate button was pressed
	jz generate

	cmp al, 0FFh ;chk if the buttons were "unpressed" even b4 the in instruction
	iret

ten:  ;adds 10 hertz to frequency
	mov bx, frequency
	add bl, 01h
	mov frequency, bx
	jmp finish
hundred:  ;adds 100 hertz to frequency
	mov bx, frequency
	add bl, 10h
	mov frequency, bx
	jmp finish
onek: ;adds 1K hertz to frequency
	mov bx, frequency
	add bh, 01h
	mov frequency, bx
	jmp finish
tenk: ;adds 10K hertz to frequency
	mov bx, frequency
	add bh, 10h
	mov frequency, bx
	jmp finish
	
sine:  ;sets the form byte to 00h if the sine button was pressed
	mov form, 00h
	jmp finish
tri:  ;sets the form byte to 01h if the tri button was pressed
	mov form, 01h
	jmp finish
square:  ;sets the form byte to 02h if the square button was pressed
	mov form, 02h
	jmp finish

generate:  ;performs necessary computations to calculate D to DAC, sets the o/p select, outs the D to DAC
	mov ax, frequency

	mov bx,ax
	mov ch,10
	
	mov dx,bx
	mov cl,12
	shr dx,cl
	mov ax,dx
	mul ch

	mov dx,bx
	mov cl,8
	and dx,0F00h
	shr dx,cl
	add ax,dx
	mul ch
	
	mov cl,4
	mov dx,bx
	and dx,00F0h
	shr dx,cl
	add ax,dx
	mul ch

	mov dx,bx
	and dx,0000Fh
	add ax,dx

	mov bx,256
	mul bx
	mov bx,9999
	div bx
	;;now ax has a value between 0-255 corresponding to the frequency
	out 82h, al  ; outs the frequency to the DAC and latches it
	
	cmp form, 00h
	jz sineout
	cmp form, 01h
	jz triout
	cmp form, 10h
	jz squareout
	
	sineout: ;reset 6th and 7th bit so that sine way is selected
	mov al, 0Ch ;BSR control word to reset 6th bit
	out 86h, al
	mov ch, 0Eh ;BSR control word to reset 7th bit
	out 86h, al
	jmp finish
	
	triout: ;set 6th and reset 7th bit so that tri way is selected
	mov al, 0Dh ;BSR control word to set 6th bit
	out 86h, al
	mov ch, 0Eh ;BSR control word to reset 7th bit
	out 86h, al
	jmp finish
	
	squareout:  ;reset 6th and set 7th bit so that tri way is selected
	mov al, 0Ch ;BSR control word to reset 6th bit
	out 86h, al
	mov ch, 0Fh ;BSR control word to set 7th bit
	out 86h, al
	jmp finish
finish:
	call DELAY  ;to ensure debounce is taken care of
	iret


end
